module State (State (..), MenuEntry (..), GameData (..),
    Board (..), Field (..), FieldType (..), Player (..),
    Figure (..), TurnData (..)) where

import Graphics.UI.GLUT hiding (Menu,rect)

data State = Game TurnData GameData | Menu String Int [MenuEntry] | Quit deriving (Show)

data MenuEntry = StateEntry String State | TextEntry String deriving (Show)

-- player (number of fields to move) [(number of throw)/(origin figure) (possible fields)]
data TurnData = Throw Int Int Int | MoveFigure Int Int
    | MoveField Int Int (Figure,Field) [(Figure,Field)] deriving (Show)
data GameData = GameData Board deriving (Show)
-- (max. players) (list of fields)
data Board = Board Int [(Figure,Field)] deriving (Show)
-- type x y (player on field)
data Field = Field FieldType GLfloat GLfloat Player deriving (Eq, Show)
data FieldType = Normal | Start | Home | Last | Out | Marker deriving (Eq, Show)
data Player = Player Int | NoPlayer deriving (Eq, Show)
data Figure = Figure Int | NoFigure deriving (Eq, Show)
