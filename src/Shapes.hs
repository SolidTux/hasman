module Shapes (rect, cubeFrame, circle, defaultCircle, bg, rgba,
    dice) where

import Graphics.UI.GLUT hiding (rect,rgba)

vertex3f :: (GLfloat, GLfloat, GLfloat) -> IO ()
vertex3f (x, y, z) = vertex $ Vertex3 x y z

dice n s x y = do
    rgba 0 0 0 1
    rect (x, y, 1.1*s, 1.1*s)
    rgba 1 1 1 1
    rect (x, y, s, s)
    rgba 0 0 0 1
    diceNumber n s x y
    rgba 1 1 1 1

diceNumber 1 s x y = defaultCircle (0.1*s) x y
diceNumber 2 s x y = do
    defaultCircle (0.1*s) (x+0.25*s) (y+0.25*s)
    defaultCircle (0.1*s) (x-0.25*s) (y-0.25*s)
diceNumber 3 s x y = do
    diceNumber 1 s x y
    diceNumber 2 s x y
diceNumber 4 s x y = do
    defaultCircle (0.1*s) (x+0.25*s) (y+0.25*s)
    defaultCircle (0.1*s) (x+0.25*s) (y-0.25*s)
    defaultCircle (0.1*s) (x-0.25*s) (y+0.25*s)
    defaultCircle (0.1*s) (x-0.25*s) (y-0.25*s)
diceNumber 5 s x y = do
    diceNumber 1 s x y
    diceNumber 4 s x y
diceNumber 6 s x y = do
    defaultCircle (0.1*s) (x+0.25*s) (y+0.25*s)
    defaultCircle (0.1*s) (x+0.25*s) y
    defaultCircle (0.1*s) (x+0.25*s) (y-0.25*s)
    defaultCircle (0.1*s) (x-0.25*s) (y+0.25*s)
    defaultCircle (0.1*s) (x-0.25*s) y
    defaultCircle (0.1*s) (x-0.25*s) (y-0.25*s)
diceNumber _ _ _ _ = return ()

--rgba r g b a = color $ Color4 r g b (a :: GLfloat)
rgba r g b a = color $ Color3 ra ga (ba :: GLfloat)
    where
        ra = min 1 (r+1-a)
        ga = min 1 (g+1-a)
        ba = min 1 (b+1-a)

rect :: (GLfloat, GLfloat, GLfloat, GLfloat) -> IO ()
rect (x,y,w,h) = renderPrimitive Polygon $ mapM_ vertex3f
    [ (x-0.5*w,y-0.5*w,0), (x+0.5*w,y-0.5*w,0), (x+0.5*w,y+0.5*h,0), (x-0.5*w,y+0.5*h,0) ]

bg = rect (0,0,2,2)

cubeFrame :: GLfloat -> IO ()
cubeFrame w = renderPrimitive Lines $ mapM_ vertex3f
  [ ( w,-w, w), ( w, w, w),  ( w, w, w), (-w, w, w),
    (-w, w, w), (-w,-w, w),  (-w,-w, w), ( w,-w, w),
    ( w,-w, w), ( w,-w,-w),  ( w, w, w), ( w, w,-w),
    (-w, w, w), (-w, w,-w),  (-w,-w, w), (-w,-w,-w),
    ( w,-w,-w), ( w, w,-w),  ( w, w,-w), (-w, w,-w),
    (-w, w,-w), (-w,-w,-w),  (-w,-w,-w), ( w,-w,-w) ]

circle :: Int -> GLfloat -> GLfloat -> GLfloat -> IO ()
circle n r x y = renderPrimitive Polygon $ mapM_ vertex3f $ circlePoints n r x y
circlePoints :: Int -> GLfloat -> GLfloat -> GLfloat
    -> [(GLfloat, GLfloat, GLfloat)]
circlePoints n r x y = [((x + r*cos(2*pi*k/nf)), (y + r*sin(2*pi*k/nf)), 0)
    | k <- map fromIntegral [1..n]]
    where
        nf = fromIntegral n
defaultCircle = circle 100
