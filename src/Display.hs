module Display (display, idle) where

import Graphics.UI.GLUT
import Data.IORef
import Shapes
import State
import StateHandling

display :: IORef State -> DisplayCallback
display state = do
    s <- get state
    clear [ ColorBuffer, AccumBuffer ]
    renderState s
    swapBuffers

idle :: IdleCallback
idle = do
    reportErrors
    postRedisplay Nothing
