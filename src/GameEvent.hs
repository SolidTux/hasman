module GameEvent (gameHandleStateEvent, getValidFields) where

import Graphics.UI.GLUT hiding (Menu,rect,Normal,rgba)
import Shapes
import State
import DefaultData
import Data.Ord
import Data.List
import System.Random

import Debug.Trace

gameHandleStateEvent :: TurnData -> GameData -> Key -> KeyState -> Modifiers
    -> GLfloat -> GLfloat -> IO State
gameHandleStateEvent td gd key Up _ posx posy = case key of
        (Char '\ESC') -> return mainMenu
        (Char 'q') -> return mainMenu
        (MouseButton LeftButton) -> handleClick td gd posx posy
        _ -> return $ Game td gd
gameHandleStateEvent td gd _ _ _ _ _ = return $ Game td gd

handleClick :: TurnData -> GameData -> GLfloat -> GLfloat -> IO State
handleClick (MoveFigure i d) gd mx my
    | nf1 == []         = return (Game (Throw np d 0) gd)
    | mindis >= 0.07    = return (Game (MoveFigure i d) gd)
    | vf == []          = return (Game (MoveFigure i d) gd)
    | otherwise         = return (Game (MoveField i d cf vf) gd)
    where
        GameData b = gd
        Board m f = b
        pf = filter (\(x,_) -> getFigureNum x == i) f
        np = head . drop 1 . dropWhile ((/=) i) . cycle
                $ playerList numPlayers m
        nf
            | d == 6    = filter (\x -> (fieldType x /= Marker)) pf
            | otherwise = filter (\x -> (fieldType x /= Marker)
                && (fieldType x /= Out)) pf
        nf1 = filter (\x -> (length $ getValidFields nf2 x d) > 0) nf
        nf2
            | d == 6    = filter (\x -> fieldType x /= Marker) f
            | otherwise = filter (\x -> (fieldType x /= Marker)
                && (fieldType x /= Out)) f
        mindis = head . sort . map dis $ nf1
        cf = head . sortBy (comparing dis) $ nf1
        dis (_,Field _ x y _) = sqrt ((x-mx)^2 + (y-my)^2)
        vf = getValidFields nf2 cf d

handleClick (MoveField i d fiel vf) gd mx my
    | mindis >= 0.07    = return $ Game (MoveField i d fiel vf) gd
    | cf == fiel        = return $ Game (MoveFigure i d) gd
    | cfi /= i          = return $ Game (Throw np d 0) (moveFigure gd fiel cf)
    where
        GameData b = gd
        Board m f = b
        mindis = head . sort . map dis $ vf
        np
            | d == 6    = i
            | otherwise = head . drop 1 . dropWhile ((/=) i) . cycle
                $ playerList numPlayers m
        cf = head . sortBy (comparing dis) $ vf
        (cfig, _) = cf
        cfi= getFigureNum cfig
        dis (_,Field _ x y _) = sqrt ((x-mx)^2 + (y-my)^2)
handleClick (Throw i d n) gd _ _ = do
    putStrLn . show $ (i,d,n)
    d2 <- randomRIO (1, 6 :: Int)
    if (nf1 d2 == []) then
        if (n < 2) then
            return $ Game (Throw i d2 (n+1)) gd
        else
            return $ Game (Throw np d2 0) gd
    else
        return $ Game (MoveFigure i d2) gd
    where
        GameData b = gd
        Board m f = b
        np = head . drop 1 . dropWhile ((/=) i) . cycle
            $ playerList numPlayers m
        pf = filter (\(x,_) -> getFigureNum x == i) f
        nf x
            | x == 6    = filter (\x -> fieldType x /= Marker) pf
            | otherwise = filter (\x -> (fieldType x /= Marker)
                && (fieldType x /= Out)) pf

        nf1 x = filter (\x -> (length $ getValidFields nf2 x d) > 0) $ nf x
        nf2
            | d == 6    = filter (\x -> fieldType x /= Marker) f
            | otherwise = filter (\x -> (fieldType x /= Marker)
                && (fieldType x /= Out)) f
handleClick td gd _ _ = return $ Game td gd

-- TODO test for other figures
getValidFields f (Figure i, Field Home x y p) d
    | hf == []                      = []
    | (fst . head $ hf) == NoFigure = hf
    | otherwise                     = []
    where
        hf = take 1 . drop d . dropWhile ((/=) (Figure i, Field Home x y p))
            . filter (\x -> fieldType x == Home)
            . filter (\(_,Field _ _ _ p) -> getPlayerNum p == i) $ f
getValidFields f (Figure i, Field Out _ _ _) 6 =
    take 1 . filter (\(_, Field t _ _ p) ->
        (p == Player i) && (t == Start)) $ f
getValidFields f (Figure i, Field Last x y p) d
    | p == Player i     = if (d > 4) then
            []
        else if (res == []) then
            []
        else if ((fst . head $ res) == NoFigure) then
            res
        else
            []
        where
            res = take 1 . drop (d-1) . filter (\(_, Field t _ _ p) ->
                (p == Player i) && (t == Home)) $ f
getValidFields f _ _ = filter (\x -> (fieldType x == Normal)
    || (fieldType x == Last) || (fieldType x == Start)) f

fieldType (_, Field t _ _ _) = t

getFigureNum (Figure i) = i
getFigureNum _ = -1

getPlayerNum NoPlayer = -1
getPlayerNum (Player i) = i

-- TODO remove other player
moveFigure :: GameData -> (Figure,Field) -> (Figure,Field) -> GameData
moveFigure (GameData (Board n f)) f1 f2
    | occu f2   = GameData (Board n $ map (mv f1 f2 . mv f2 out2) f)
    | otherwise = GameData (Board n $ map (mv f1 f2) f)
    where
        occu (Figure _,_) = True
        occu _ = False
        (fig,_) = f2
        i2 = getFigureNum fig
        out2 = head . filter (\(x,_) -> x == NoFigure)
            . filter (\x -> fieldType x == Out)
            . filter (\(_, Field _ _ _ p) -> getPlayerNum p == i2) $ f
        mv (fig1,fie1) (fig2,fie2) (fig,fie)
            | fie == fie1   = (NoFigure,fie)
            | fie == fie2   = (fig1,fie)
            | otherwise     = (fig,fie)
