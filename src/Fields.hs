module Fields (defaultFields4, testFields) where

import Graphics.UI.GLUT hiding (Menu,rect,Normal)
import State

testFields :: [(Figure,Field)]
testFields = [
    (NoFigure, Field Normal (-0.5) (-0.5) NoPlayer),
    (NoFigure, Field Normal (-0.5) (0.5) NoPlayer),
    (Figure 1, Field Normal (0.5) (-0.5) NoPlayer),
    (NoFigure, Field Normal (0.5) (0.5) NoPlayer)
    ]

defaultFields4 :: [(Figure,Field)]
defaultFields4 = map i2f [
    --Field Marker (-4.5) (4.5) (Player 0),
    --Field Marker (4.5) (4.5) (Player 1),
    --Field Marker (4.5) (-4.5) (Player 2),
    --Field Marker (-4.5) (-4.5) (Player 3),
    Field Marker (-2.5) (2.5) (Player 0),
    Field Marker (2.5) (2.5) (Player 1),
    Field Marker (2.5) (-2.5) (Player 2),
    Field Marker (-2.5) (-2.5) (Player 3),
    Field Start (-5) 1 (Player 0),
    Field Normal (-4) 1 NoPlayer,
    Field Normal (-3) 1 NoPlayer,
    Field Normal (-2) 1 NoPlayer,
    Field Normal (-1) 1 NoPlayer,
    Field Normal (-1) 2 NoPlayer,
    Field Normal (-1) 3 NoPlayer,
    Field Normal (-1) 4 NoPlayer,
    Field Normal (-1) 5 NoPlayer,
    Field Last 0 5 (Player 1),
    Field Start 1 5 (Player 1),
    Field Normal 1 4 NoPlayer,
    Field Normal 1 3 NoPlayer,
    Field Normal 1 2 NoPlayer,
    Field Normal 1 1 NoPlayer,
    Field Normal 2 1 NoPlayer,
    Field Normal 3 1 NoPlayer,
    Field Normal 4 1 NoPlayer,
    Field Normal 5 1 NoPlayer,
    Field Last 5 0 (Player 2),
    Field Start 5 (-1) (Player 2),
    Field Normal 4 (-1) NoPlayer,
    Field Normal 3 (-1) NoPlayer,
    Field Normal 2 (-1) NoPlayer,
    Field Normal 1 (-1) NoPlayer,
    Field Normal 1 (-2) NoPlayer,
    Field Normal 1 (-3) NoPlayer,
    Field Normal 1 (-4) NoPlayer,
    Field Normal 1 (-5) NoPlayer,
    Field Last 0 (-5) (Player 3),
    Field Start (-1) (-5) (Player 3),
    Field Normal (-1) (-4) NoPlayer,
    Field Normal (-1) (-3) NoPlayer,
    Field Normal (-1) (-2) NoPlayer,
    Field Normal (-1) (-1) NoPlayer,
    Field Normal (-2) (-1) NoPlayer,
    Field Normal (-3) (-1) NoPlayer,
    Field Normal (-4) (-1) NoPlayer,
    Field Normal (-5) (-1) NoPlayer,
    Field Last (-5) 0 (Player 0),
    Field Start (-5) 1 (Player 0),
    Field Home (-4) 0 (Player 0),
    Field Home (-3) 0 (Player 0),
    Field Home (-2) 0 (Player 0),
    Field Home (-1) 0 (Player 0),
    Field Home 0 4 (Player 1),
    Field Home 0 3 (Player 1),
    Field Home 0 2 (Player 1),
    Field Home 0 1 (Player 1),
    Field Home 4 0 (Player 2),
    Field Home 3 0 (Player 2),
    Field Home 2 0 (Player 2),
    Field Home 1 0 (Player 2),
    Field Home 0 (-4) (Player 3),
    Field Home 0 (-3) (Player 3),
    Field Home 0 (-2) (Player 3),
    Field Home 0 (-1) (Player 3),
    Field Out (-5) 5 (Player 0),
    Field Out (-5) 4 (Player 0),
    Field Out (-4) 5 (Player 0),
    Field Out (-4) 4 (Player 0),
    Field Out 5 5 (Player 1),
    Field Out 5 4 (Player 1),
    Field Out 4 5 (Player 1),
    Field Out 4 4 (Player 1),
    Field Out 5 (-5) (Player 2),
    Field Out 5 (-4) (Player 2),
    Field Out 4 (-5) (Player 2),
    Field Out 4 (-4) (Player 2),
    Field Out (-5) (-5) (Player 3),
    Field Out (-5) (-4) (Player 3),
    Field Out (-4) (-5) (Player 3),
    Field Out (-4) (-4) (Player 3)
    ]
    where
        i2f (Field t xi yi p) = (NoFigure,Field t (xi*0.9/5) (yi*0.9/5) p)
