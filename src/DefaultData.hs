module DefaultData (mainMenu, defaultGameData, font, defaultGame,
    numPlayers, playerList) where

import Graphics.UI.GLUT hiding (rect, Menu)
import System.Random
import Data.IORef
import State
import Fields

font = Helvetica18
numPlayers = 2
playerList n m = take n [ x*(m `div` n) | x<-[0..]]

mainMenu = Menu "Main" 0 [s,q]
    where
        s = StateEntry "Start" defaultGame
        q = StateEntry "Quit" Quit

defaultGame = Game (Throw 0 1 0) (foldl addPlayerStart defaultGameData (playerList numPlayers 4))
defaultGameData = GameData $ Board 4 defaultFields4
--defaultGameData = GameData $ Board 4 testFields

-- TODO test for max players
addPlayerStart (GameData (Board n f)) i = GameData $ Board n (map addp f)
    where
        addp (NoFigure, Field Out x y (Player p))
            | p == i    = (Figure p, Field Out x y (Player p))
        addp x = x
