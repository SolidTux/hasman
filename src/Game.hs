module Game (gameHandleStateEvent, gameRenderState) where

import Graphics.UI.GLUT hiding (Menu,rect,Normal,rgba)
import State
import Debug.Trace
import Shapes
import System.Random
import DefaultData
import GameEvent
import GameRender
