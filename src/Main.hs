import Graphics.UI.GLUT
import Data.IORef

import Bindings
import DefaultData

main :: IO ()
main = do
    (prog, args) <- getArgsAndInitialize
    initialDisplayMode $= [DoubleBuffered,WithAlphaComponent,RGBAMode]
    win <- createWindow "Test"
    --state <- newIORef mainMenu
    state <- newIORef defaultGame
    displayCallback $= display state
    reshapeCallback $= Just reshape
    keyboardMouseCallback $= Just (keyboardMouse state)
    idleCallback $= Just idle
    mainLoop
