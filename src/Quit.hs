module Quit (quit, Quit (..)) where

import State
import System.Exit

data Quit = Quit {}

instance State Quit where
    renderState _ = exitSuccess
    handleStateEvent s _ _ _ _ = s

quit = Quit {}
