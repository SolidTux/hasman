module Menu(mainMenu, menuRenderState, menuHandleStateEvent) where

import Graphics.UI.GLUT hiding (Menu,rect)
import Debug.Trace
import State
import DefaultData
import Game
import Shapes

menuRenderState t sel e = do
    loadIdentity
    rasterPos $ Vertex2 0 (0.8 :: GLfloat)
    renderString font t
    mapM_ (renderMenuEntry sel len) $ zip [0..] e
    where
        len = length e

action (StateEntry _ s) = s

renderMenuEntry s n (i,StateEntry t _) = renderMenuEntry s n (i,TextEntry t)
renderMenuEntry s n (i,TextEntry t) = do
    entryColor (i==s)
    rect (x,y,w,h)
    color $ Color3 1 1 (1 :: GLfloat)
    rasterPos $ Vertex2 0 (y + 0.5*h :: GLfloat)
    renderString font t
    where
        w = 1
        h = 0.2
        mar = 0.1
        x = -0.5*w
        n2 = fromIntegral n
        i2 = fromIntegral i
        y = ((n2-1)/2-i2)*(h+mar)

entryColor x
    | x         = color $ Color3 1 0 (0 :: GLfloat)
    | otherwise = color $ Color3 0 1 (0 :: GLfloat)

menuHandleStateEvent t s e key Up _ _ _ = do --trace (show key) $ do
    case key of
        (SpecialKey KeyUp) -> Menu t (max 0 (s-1)) e
        (SpecialKey KeyDown) -> Menu t (min (len-1) (s+1)) e
        (Char '\ESC') -> Quit
        (Char 'q') -> Quit
        (Char '\r') -> case (e!!s) of
            (StateEntry _ st) -> st
            _ -> Menu t s e
        _ -> Menu t s e
        where
            len = length e
menuHandleStateEvent t s e _ _ _ _ _ = Menu t s e
