module StateHandling (renderState, handleStateEvent) where

import System.Exit
import Graphics.UI.GLUT hiding (Menu)
import State
import Menu
import Game

renderState :: State -> IO ()
renderState (Menu t s e) = menuRenderState t s e
renderState (Game td gd)    = gameRenderState td gd
renderState Quit         = do
    putStrLn "Exiting"
    exitSuccess

handleStateEvent :: State -> Key -> KeyState -> Modifiers -> GLfloat -> GLfloat
    -> IO State
handleStateEvent (Menu t s e) a b c posx posy =
    return $ menuHandleStateEvent t s e a b c posx posy
handleStateEvent (Game td gd) a b c posx posy =
    gameHandleStateEvent td gd a b c posx posy
handleStateEvent Quit _ _ _ _ _ = return Quit
