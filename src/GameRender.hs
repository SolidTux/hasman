module GameRender (gameRenderState) where

import Graphics.UI.GLUT hiding (Menu,rect,Normal,rgba)
import Shapes
import State
import GameEvent

fullViewport :: IO ()
fullViewport = do
    v <- get viewport
    let (p,_) = v
    s <- get windowSize
    viewport $= (Position 0 0, s)

boardViewport mar = do
    size <- get windowSize
    viewport $= getBoardView mar size

getBoardView mar size = (Position posx posy, Size s s)
    where
        Size x y = size
        s = (min x y) - 2*mar
        posx = (x - s) `div` 2
        posy = (y - s) `div` 2

gameRenderState td (GameData b) = do
    fullViewport
    rgba 0 0.2 0 1
    bg
    boardViewport 0
    --rgba 0.722 0.565 0.098 1
    rgba 1 1 0 0.3
    bg
    let i = getCurrentPlayer td
    rgba 1 1 1 1
    mapM_ (renderField i) f
    renderTurnData td (GameData b)
    where
        Board _ f = b

renderTurnData (Throw i d _) gd = do
    rgba 1 0 0 0.8
    defaultCircle 0.2 x y
    renderDice i d gd
    where
        GameData b = gd
        Board m f = b
        pf = filter (\(_,Field _ _ _ x) -> getPlayerNum x == i) f
        (_,mar) = head . filter (\x -> fieldType x == Marker) $ pf
        Field _ x y _ = mar
renderTurnData (MoveFigure i d) gd = do
    renderDice i d gd
    mapM_ renderValidField nf1
    where
        GameData b = gd
        Board m f = b
        pf = filter (\(x,_) -> getFigureNum x == i) f
        nf
            | d == 6    = filter (\x -> fieldType x /= Marker) pf
            | otherwise = filter (\x -> (fieldType x /= Marker)
                && (fieldType x /= Out)) pf
        nf1 = filter (\x -> (length $ getValidFields nf2 x d) > 0) nf
        nf2
            | d == 6    = filter (\x -> fieldType x /= Marker) f
            | otherwise = filter (\x -> (fieldType x /= Marker)
                && (fieldType x /= Out)) f
renderTurnData (MoveField _ d (Figure i, Field _ x y _) vf) gd = do
    renderDice i d gd
    rgba 0 1 1 1
    defaultCircle 0.04 x y
    playerColor (Player i) 1
    defaultCircle 0.03 x y
    rgba 1 1 1 1
    mapM_ renderValidField vf
renderTurnData (MoveField i d _ _) gd = do
    renderDice i d gd

fieldType (_, Field t _ _ _) = t
getFigureNum (Figure i) = i
getFigureNum _ = -1

renderDice i d gd = do
    dice d 0.2 x y
    where
        GameData b = gd
        Board m f = b
        pf = filter (\(_,Field _ _ _ x) -> getPlayerNum x == i) f
        (_,mar) = head . filter (\x -> fieldType x == Marker) $ pf
        Field _ x y _ = mar


renderValidField (_, Field _ x y _) = do
    rgba 0 0 0 1
    defaultCircle 0.01 x y
    rgba 1 1 1 1


getCurrentPlayer (Throw i _ _) = i
getCurrentPlayer (MoveFigure i _) = i
getCurrentPlayer (MoveField i _ _ _) = i

getPlayerNum NoPlayer = -1
getPlayerNum (Player i) = i

fieldColor Out p = playerColor p 1
fieldColor Home p = playerColor p 1
fieldColor Last p = playerColor p 0.3
fieldColor Start p = playerColor p 0.5
fieldColor _ _ = rgba 1 1 1 1

playerColor (Player 0) a = rgba 1 0 0 a
playerColor (Player 1) a = rgba 0 1 0 a
playerColor (Player 2) a = rgba 0 0 1 a
playerColor (Player 3) a = rgba 1 1 0 a
playerColor _ a = rgba 1 1 1 a

renderField i (_, (Field Marker x y p)) = return ()
renderField _ (fig, (Field f x y p)) = do
    rgba 0 0 0 1
    defaultCircle 0.06 x y
    fieldColor f p
    defaultCircle 0.05 x y
    rgba 1 1 1 1
    renderFig fig x y

renderFig NoFigure _ _ = return ()
renderFig (Figure i) x y = do
    rgba 0 0 0 1
    defaultCircle 0.04 x y
    playerColor (Player i) 1
    defaultCircle 0.03 x y
    rgba 1 1 1 1
