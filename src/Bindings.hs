module Bindings (display, reshape, keyboardMouse, idle) where

import Display
import Graphics.UI.GLUT
import Data.IORef
import State
import StateHandling

reshape :: ReshapeCallback
reshape size = do
    postRedisplay Nothing

keyboardMouse :: IORef State -> KeyboardMouseCallback
keyboardMouse state key act mod pos = do
    s <- get state
    v <- get viewport
    let (posx, posy) = pos2v v pos
    s2 <- handleStateEvent s key act mod posx posy
    state $= s2
    return ()
pos2v v pos = (posx,posy)
    where
        (Position vpx vpy, Size sx sy) = v
        Position px py = pos
        posx = 2*(fromIntegral (px-vpx))/(fromIntegral sx) - 1
        posy = -2*(fromIntegral (py-vpy))/(fromIntegral sy) + 1
